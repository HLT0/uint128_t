/*
uint128_t.h
An unsigned 128 bit integer type for C++

Copyright (c) 2013 - 2017 Jason Lee @ calccrypto at gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

With much help from Auston Sterling

Thanks to Stefan Deigm�ller for finding
a bug in operator*.

Thanks to Fran�ois Dessenne for convincing me
to do a general rewrite of this class.
*/

#ifndef __UINT128_T__
#define __UINT128_T__

#ifndef ENABLE_IF_Z
#if __cplusplus >= 201402L   // C++14
#define ENABLE_IF_Z(T) std::enable_if_t <std::is_integral<T>::value>
#elif __cplusplus >= 201103L // C++11
#define ENABLE_IF_Z(T) typename std::enable_if <std::is_integral<T>::value, T>::type
#else
#error uint128_t requires at least C++11 to compile
#endif
#endif

#include <climits>
#include <cmath>
#include <cstdint>
#include <limits>
#include <ostream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

class uint128_t;

// Give uint128_t type traits
namespace std {  // This is probably not a good idea
    template <> struct is_arithmetic <uint128_t> : std::true_type {};
    template <> struct is_integral   <uint128_t> : std::true_type {};
    template <> struct is_unsigned   <uint128_t> : std::true_type {};
};

class uint128_t{
    private:
        uint64_t UPPER, LOWER;

    public:
        // Constructors
        constexpr uint128_t()
            : UPPER(0), LOWER(0)
        {}

        constexpr uint128_t(const uint128_t & rhs)
            : UPPER(rhs.UPPER), LOWER(rhs.LOWER)
        {}

        constexpr uint128_t(uint128_t && rhs)
            : UPPER(std::move(rhs.UPPER)), LOWER(std::move(rhs.LOWER))
        {
            if (this != &rhs){
                rhs.UPPER = 0;
                rhs.LOWER = 0;
            }
        }

        template <typename T, typename = ENABLE_IF_Z(T)>
        constexpr uint128_t(const T & rhs)
            : UPPER(0), LOWER(rhs)
        {}

        template <typename S, typename T, typename = ENABLE_IF_Z(S), typename = ENABLE_IF_Z(T)>
        constexpr uint128_t(const S & upper_rhs, const T & lower_rhs)
            : UPPER(upper_rhs), LOWER(lower_rhs)
        {}

        //  RHS input args only

        // Assignment Operator
        uint128_t & operator=(const uint128_t & rhs);
        uint128_t & operator=(uint128_t && rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator=(const T & rhs){
            UPPER = 0;
            LOWER = rhs;
            return *this;
        }

        // Typecast Operators
        operator bool() const;
        operator uint8_t() const;
        operator uint16_t() const;
        operator uint32_t() const;
        operator uint64_t() const;

        // Bitwise Operators
        uint128_t operator&(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator&(const T & rhs) const{
            return uint128_t(0, LOWER & (uint64_t) rhs);
        }

        uint128_t & operator&=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator&=(const T & rhs){
            UPPER = 0;
            LOWER &= rhs;
            return *this;
        }

        uint128_t operator|(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator|(const T & rhs) const{
            return uint128_t(UPPER, LOWER | (uint64_t) rhs);
        }

        uint128_t & operator|=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator|=(const T & rhs){
            LOWER |= (uint64_t) rhs;
            return *this;
        }

        uint128_t operator^(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator^(const T & rhs) const{
            return uint128_t(UPPER, LOWER ^ (uint64_t) rhs);
        }

        uint128_t & operator^=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator^=(const T & rhs){
            LOWER ^= (uint64_t) rhs;
            return *this;
        }

        uint128_t operator~() const;

        // Bit Shift Operators
        uint128_t operator<<(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator<<(const T & rhs) const{
            return *this << uint128_t(rhs);
        }

        uint128_t & operator<<=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator<<=(const T & rhs){
            *this = *this << uint128_t(rhs);
            return *this;
        }

        uint128_t operator>>(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator>>(const T & rhs) const{
            return *this >> uint128_t(rhs);
        }

        uint128_t & operator>>=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator>>=(const T & rhs){
            *this = *this >> uint128_t(rhs);
            return *this;
        }

        // Logical Operators
        bool operator!() const;
        bool operator&&(const uint128_t & rhs) const;
        bool operator||(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator&&(const T & rhs){
            return static_cast <bool> (*this && rhs);
        }

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator||(const T & rhs){
            return static_cast <bool> (*this || rhs);
        }

        // Comparison Operators
        bool operator==(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator==(const T & rhs) const{
            return (!UPPER && (LOWER == (uint64_t) rhs));
        }

        bool operator!=(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator!=(const T & rhs) const{
            return (UPPER | (LOWER != (uint64_t) rhs));
        }

        bool operator>(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator>(const T & rhs) const{
            return (UPPER || (LOWER > (uint64_t) rhs));
        }

        bool operator<(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator<(const T & rhs) const{
            return (!UPPER)?(LOWER < (uint64_t) rhs):false;
        }

        bool operator>=(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator>=(const T & rhs) const{
            return ((*this > rhs) | (*this == rhs));
        }

        bool operator<=(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator<=(const T & rhs) const{
            return ((*this < rhs) | (*this == rhs));
        }

        // Arithmetic Operators
        uint128_t operator+(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator+(const T & rhs) const{
            return uint128_t(UPPER + ((LOWER + (uint64_t) rhs) < LOWER), LOWER + (uint64_t) rhs);
        }

        uint128_t & operator+=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator+=(const T & rhs){
            UPPER = UPPER + ((LOWER + rhs) < LOWER);
            LOWER = LOWER + rhs;
            return *this;
        }

        uint128_t operator-(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator-(const T & rhs) const{
            return uint128_t((uint64_t) (UPPER - ((LOWER - rhs) > LOWER)), (uint64_t) (LOWER - rhs));
        }

        uint128_t & operator-=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator-=(const T & rhs){
            *this = *this - rhs;
            return *this;
        }

        uint128_t operator*(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator*(const T & rhs) const{
            return *this * uint128_t(rhs);
        }

        uint128_t & operator*=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator*=(const T & rhs){
            *this = *this * uint128_t(rhs);
            return *this;
        }

    private:
        std::pair <uint128_t, uint128_t> divmod(const uint128_t & lhs, const uint128_t & rhs) const;

    public:
        uint128_t operator/(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator/(const T & rhs) const{
            return *this / uint128_t(rhs);
        }

        uint128_t & operator/=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator/=(const T & rhs){
            *this = *this / uint128_t(rhs);
            return *this;
        }

        uint128_t operator%(const uint128_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t operator%(const T & rhs) const{
            return *this % uint128_t(rhs);
        }

        uint128_t & operator%=(const uint128_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint128_t & operator%=(const T & rhs){
            *this = *this % uint128_t(rhs);
            return *this;
        }

        // Increment Operator
        uint128_t & operator++();
        uint128_t operator++(int);

        // Decrement Operator
        uint128_t & operator--();
        uint128_t operator--(int);

        // Nothing done since promotion doesn't work here
        uint128_t operator+() const;

        // two's complement
        uint128_t operator-() const;

        // Get private values
        const uint64_t & upper() const;
        const uint64_t & lower() const;

        // Get bitsize of value
        uint8_t bits() const;

        // Get string representation of value
        std::string str(uint8_t base = 10, const unsigned int & len = 0) const;
};

// lhs type T as first arguemnt
// If the output is not a bool, casts to type T

// Bitwise Operators
template <typename T, typename = ENABLE_IF_Z(T)>
uint128_t operator&(const T & lhs, const uint128_t & rhs){
    return rhs & lhs;
}

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator&=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (rhs & lhs);
}

template <typename T, typename = ENABLE_IF_Z(T)>
uint128_t operator|(const T & lhs, const uint128_t & rhs){
    return rhs | lhs;
}

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator|=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (rhs | lhs);
}

template <typename T, typename = ENABLE_IF_Z(T)>
uint128_t operator^(const T & lhs, const uint128_t & rhs){
    return rhs ^ lhs;
}

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator^=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (rhs ^ lhs);
}

// Bitshift operators
uint128_t operator<<(const bool     & lhs, const uint128_t & rhs);
uint128_t operator<<(const uint8_t  & lhs, const uint128_t & rhs);
uint128_t operator<<(const uint16_t & lhs, const uint128_t & rhs);
uint128_t operator<<(const uint32_t & lhs, const uint128_t & rhs);
uint128_t operator<<(const uint64_t & lhs, const uint128_t & rhs);
uint128_t operator<<(const int8_t   & lhs, const uint128_t & rhs);
uint128_t operator<<(const int16_t  & lhs, const uint128_t & rhs);
uint128_t operator<<(const int32_t  & lhs, const uint128_t & rhs);
uint128_t operator<<(const int64_t  & lhs, const uint128_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator<<=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (uint128_t(lhs) << rhs);
}

uint128_t operator>>(const bool     & lhs, const uint128_t & rhs);
uint128_t operator>>(const uint8_t  & lhs, const uint128_t & rhs);
uint128_t operator>>(const uint16_t & lhs, const uint128_t & rhs);
uint128_t operator>>(const uint32_t & lhs, const uint128_t & rhs);
uint128_t operator>>(const uint64_t & lhs, const uint128_t & rhs);
uint128_t operator>>(const int8_t   & lhs, const uint128_t & rhs);
uint128_t operator>>(const int16_t  & lhs, const uint128_t & rhs);
uint128_t operator>>(const int32_t  & lhs, const uint128_t & rhs);
uint128_t operator>>(const int64_t  & lhs, const uint128_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator>>=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (uint128_t(lhs) >> rhs);
}

// Comparison Operators
template <typename T, typename = ENABLE_IF_Z(T)>
bool operator==(const T & lhs, const uint128_t & rhs){
    return (!rhs.upper() && ((uint64_t) lhs == rhs.lower()));
}

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator!=(const T & lhs, const uint128_t & rhs){
    return (rhs.upper() | ((uint64_t) lhs != rhs.lower()));
}

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator>(const T & lhs, const uint128_t & rhs){
    return (!rhs.upper()) && ((uint64_t) lhs > rhs.lower());
}

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator<(const T & lhs, const uint128_t & rhs){
    if (rhs.upper()){
        return true;
    }
    return ((uint64_t) lhs < rhs.lower());
}

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator>=(const T & lhs, const uint128_t & rhs){
    if (rhs.upper()){
        return false;
    }
    return ((uint64_t) lhs >= rhs.lower());
}

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator<=(const T & lhs, const uint128_t & rhs){
    if (rhs.upper()){
        return true;
    }
    return ((uint64_t) lhs <= rhs.lower());
}

// Arithmetic Operators
template <typename T, typename = ENABLE_IF_Z(T)>
uint128_t operator+(const T & lhs, const uint128_t & rhs){
    return rhs + lhs;
}

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator+=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (rhs + lhs);
}

template <typename T, typename = ENABLE_IF_Z(T)>
uint128_t operator-(const T & lhs, const uint128_t & rhs){
    return -(rhs - lhs);
}

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator-=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (-(rhs - lhs));
}

template <typename T, typename = ENABLE_IF_Z(T)>
uint128_t operator*(const T & lhs, const uint128_t & rhs){
    return rhs * lhs;
}

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator*=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (rhs * lhs);
}

template <typename T, typename = ENABLE_IF_Z(T)>
uint128_t operator/(const T & lhs, const uint128_t & rhs){
    return uint128_t(lhs) / rhs;
}

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator/=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (uint128_t(lhs) / rhs);
}

template <typename T, typename = ENABLE_IF_Z(T)>
uint128_t operator%(const T & lhs, const uint128_t & rhs){
    return uint128_t(lhs) % rhs;
}

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator%=(T & lhs, const uint128_t & rhs){
    return lhs = static_cast <T> (uint128_t(lhs) % rhs);
}

// IO Operator
std::ostream & operator<<(std::ostream & stream, const uint128_t & rhs);

#if __cplusplus >= 201402L
// Give uint128_t numeric limits in C++14 only
namespace std {  // This is probably not a good idea
    template <>
    class numeric_limits <uint128_t> {
        public:
            static constexpr bool is_specialized = true;
            static constexpr bool is_signed = false;
            static constexpr bool is_integer = true;
            static constexpr bool is_exact = true;
            static constexpr bool has_infinity = false;
            static constexpr bool has_quiet_NaN = false;
            static constexpr bool has_signaling_NaN = false;
            static constexpr std::float_denorm_style has_denorm = std::denorm_absent;
            static constexpr bool has_denorm_loss = false;
            static constexpr std::float_round_style round_style = std::round_toward_zero;
            static constexpr bool is_iec559 = false;
            static constexpr bool is_bounded = true;
            static constexpr bool is_modulo = true;
            static constexpr int digits = CHAR_BIT*(sizeof(uint64_t) + sizeof(uint64_t));
            static constexpr int digits10 = std::numeric_limits<uint128_t>::digits * std::log10(2);
            static constexpr int max_digits10 = 0;
            static constexpr int radix = 2;
            static constexpr int min_exponent = 0;
            static constexpr bool min_exponent10 = 0;
            static constexpr int max_exponent = 0;
            static constexpr bool max_exponent10 = 0;
            static constexpr bool traps = true;
            static constexpr bool tinyness_before = false;

            static constexpr uint128_t min() {
                return 0;
            }

            static constexpr uint128_t lowest() {
                return 0;
            }

            static constexpr uint128_t max() {
                return uint128_t((uint64_t) 0xffffffffffffffffULL, (uint64_t) 0xffffffffffffffffULL);
            }

            static constexpr uint128_t epsilon() {
                return 0;
            }

            static constexpr uint128_t rounding_error() {
                return 0;
            }

            static constexpr uint128_t infinity() {
                return 0;
            }

            static constexpr uint128_t quiet_NaN() {
                return 0;
            }

            static constexpr uint128_t signaling_NaN() {
                return 0;
            }

            static constexpr uint128_t denorm_min() {
                return 0;
            }
    };
}
#endif

#endif
